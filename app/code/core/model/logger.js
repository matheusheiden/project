'use strict'

const path = require('path')

const DEFAULT_LOGING_PATH = path.dirname(require.main.filename);

var winston = require('winston')

class Logger {

    constructor () {
        this.logUtil = new winston.Logger({
        transports: [
            new winston.transports.Console({
            handleExceptions: true,
            })
        ],
        exitOnError: false
        });
    }
     log (message) {
         this.logUtil.log('info', message)
    }

}

module.exports = Logger