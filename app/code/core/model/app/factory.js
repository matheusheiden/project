'use strict'

const DEFAULT_MODEL_PATH = 'model/'

const DEFAULT_CONTROLLER_PATH = 'controller/'


class Factory {

    constructor () {
        this._singletonRegistry = []
    }


    /**
     * Default structure for requesting a model namespace/model_name
     * @param {*} modelName 
     */
    getModel(modelName, params) {
        
        let Model = this.getClass(modelName)
        return new Model(params)
    }
    /**
     * Creates a singleton instance of an object
     * @param {*} modelName 
     */
    getSingleton(modelName) {
        if (!this._singletonRegistry[modelName]) {
            let model = this.getModel(modelName, [])
            this._singletonRegistry[modelName] = model
        }
        
        return this._singletonRegistry[modelName]
    }
    /**
     * Returns model's class
     * @param {*} modelName 
     */
    getClass(modelName, type='model') {
        modelName = modelName.split('/')
        let module = modelName[0], 
        model = modelName[1]
        
        model = model.replace('_', '/')
        let requirePath = '';
        switch (type) {
            case 'model':
                requirePath = './app/code/'+module+"/"+DEFAULT_MODEL_PATH+model+'.js'
                break
            case 'controller':
                requirePath = './app/code/'+module+"/"+DEFAULT_CONTROLLER_PATH+model+'.js'
                break
        }
        console.log(requirePath);
        let Model = require.main.require(requirePath)

        return Model
    }

    getController (controllerName, request, response) {
        let Controller = this.getClass(controllerName, 'controller')
        return new Controller(request, response)
    }
}

module.exports = Factory