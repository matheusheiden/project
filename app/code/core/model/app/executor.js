'use strict'

const Async = require("async");

class Executor {

    constructor(tasks) {
        this.tasks = tasks
        this.flags = []
    }

    executeTasks(flags) {
        Project.log("executing tasks")

        let defaultScopes = ['global']

        if (Project.getApp().scope != 'global') {
            defaultScopes.push(Project.getApp().scope)
        }
        let tasksToExecute = []

        defaultScopes.forEach((scope) => {
            let scopeData = Project.getApp().module_data[scope]
            if (scopeData.hasOwnProperty('async_tasks')) {
                Object.assign(tasksToExecute, scopeData.async_tasks)
            }
        })
        console.log(tasksToExecute);
        let series = []
        tasksToExecute.forEach((task) => {
            let model = Project.getModel(task.moduleName + '/' + task.class)
            model.taskId = task.moduleName+'_'+task.class
            try {
                series.push({
                    'model': model,
                    'method' : task.method
                })

            } catch (err) {
                Project.log(err)
            }
        })
        
        series.forEach(function (task) {
            
            //if (task.model.hasOwnProperty('execute')) {
                task.model.execute(task.method)
            //}
        });
    }
    /**
     * 
     * @param {*} flag 
     * @return boolean
     */
    hasFlag(flag) {
        return (this.flags.indexOf(flag) != -1)
    }
}

module.exports = Executor