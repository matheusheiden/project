'use strict'

const FileReader = require('fs')

const Path = require('path')

const DEFAULT_MODULE_PATH = 'app/code';

const DEFAULT_CONFIG_PATH = 'app/etc'

const DEFAULT_CONFIG_FILENAME = 'config.json'

const DEFAULT_MODULES_CONTAINER = 'app/etc/modules/'

class Config {
    constructor() {
        Project.log("Initiating configuration instance")
        this.modules = []
        this.preparePaths()
        this.prepareModules().then(function () {
            Project.dispatchEvent('modules_prepared')
        }) 
    }

    preparePaths () {
        let mainFile = require.main.filename
        this.baseDir = Path.parse(mainFile).dir
    }

    /**
     * module loading is made async, but we have to handle dependeces
     */
    async prepareModules () {
        var self = this
        
        Project.log("Preparing Modules")

        let modules = this._listModules()

        modules.forEach(function(module) {
            self._loadModuleIntoApplication(module).then (function (loadResult){
                Project.dispatchEvent("module_"+loadResult+"_prepared", self)
            });
        });

        return this
    }
    /**
     * lists all modules to be loaded
     */
    _listModules () {

        let moduleList = []
        FileReader.readdirSync(this.baseDir+"/"+DEFAULT_MODULES_CONTAINER).forEach(file => {
            moduleList.push(
                this.baseDir+"/"+DEFAULT_MODULES_CONTAINER+file
            )
        })

        return moduleList
    }
    /**
     * Loads any modules inside etc/modules
     * @param {*} module 
     */
    async _loadModuleIntoApplication(module) {
        var self = this
        var result = false
        result = FileReader.readFileSync(module, 'utf8');
        if (result) {
            let parsedData = JSON.parse(result)
            if (!parsedData.module.active) { //module is not active
                result =  false
            }
            self._addModule(parsedData)
            result = parsedData.module.name
        }
        else {
            result =  new Error("Invalid module was not loaded")    
        }

        return result
    }

    _addModule (moduleData) {
        this.modules.push(moduleData.module)
    }

    getModuleConfig (module) {
        let content = FileReader.readFileSync(this.baseDir+"/"+DEFAULT_MODULE_PATH+"/"+module+"/etc/"+DEFAULT_CONFIG_FILENAME, "utf8")
        return JSON.parse(content)
    }

    async loadAppConfiguration() {
        
    }
}

module.exports = Config