'use strict'
/**
 * @author Matheus Heiden
 */
class Task {

    construct(taskId) {
        this.taskId = taskId //task id 
    }

    preDispatch() {

        Project.dispatchEvent('pre_dispatch_task', this)
        Project.dispatchEvent('pre_dispatch_task_'+this.taskId, this)



    }

    postDispatch() {
        Project.dispatchEvent('post_dispatch_task', this)
        Project.dispatchEvent('post_dispatch_task_'+this.taskId, this)
    }

    /**
     * Executes the task 
     * @return Task
     * @param {function} method 
     */
    async execute (method) {
        this.preDispatch() //launches pre dispatch event

        let ret = this[method]() //executes the method
        
        if (ret instanceof Promise) { //if is async
            ret.then(() => {
                this.postDispatch();
            })
        }
        else {
            this.postDispatch() //launches post dispatch
        }
        
        return this //piggy back if needed
    }


}

module.exports = Task