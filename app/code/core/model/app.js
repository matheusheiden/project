'use strict'

const Logger = require.main.require('./app/code/core/model/logger.js')

const Config = require.main.require('./app/code/core/model/app/config.js')

const Factory = require.main.require('./app/code/core/model/app/factory.js')

const Executor = require.main.require('./app/code/core/model/app/executor.js')

const Database = require.main.require('./app/code/core/model/resource/database.js')


const EventEmitter = require('events');



class App {
    
    start() {
        var self = this
        
        this.dispatcher = new EventEmitter()

        this.database = new Database()

        this.factory = new Factory()

        this.dispatcher.on('modules_prepared', (event) => {
            self.loadModules()
            self.dispatchEvent('modules_loaded')
        })

        this.dispatcher.on('modules_loaded', (event) => {
            self.executeTasks()
        });

        this.scope = 'global'
        
        this.config = new Config()

        
    }

    getLogger() {
        if (!this.logger) {
            this.logger = new Logger()
        }
        return this.logger
    }

    log(message) {
        this.getLogger().log(message)
    }

    dispatchEvent (eventName, data) {
        this.log("Dispatching event:'"+eventName+"'")
        this.dispatcher.emit(eventName, data);
    }

    getConfig (attr) {
        if (this.config.hasOwnProperty(attr)) {
            return this.config[attr]
        }
        return null
    }

    loadModules() {
        Project.log("loading modules")
        var self = this
        let modules = this.getConfig('modules')
        this.module_data = {}
        for (let key in modules) {
            let module = modules[key]
            if (module.active) {
                this._loadModule(module.name).then (function (moduleConfig) {
                    self.dispatchEvent('after_module_'+module.name+"_loaded", moduleConfig)
                })
            }
        }


    }

    async _loadModule(moduleName) {
        let config = this.config.getModuleConfig(moduleName)
        for (let key in config) {
            this._addModuleConfigByScope(key, config[key], moduleName)
        }
    }

    _addModuleConfigByScope(scope, config, moduleName) {
        if (!this.module_data.hasOwnProperty(scope)) {
            this.module_data[scope] = {}
        }
        for (let key in config) {
            for (let index in config[key] ) {
                config[key][index].moduleName = moduleName
            }
        }
        

        Object.assign(this.module_data[scope], config);
        
    }

    executeTasks() {
        let executor = new Executor();

        executor.executeTasks([]);
        
    }

    getTasksForCurrentScope() {

    }

    getTasksForScope(scope) {
        if (this.module_data[scope]) {
            
        }
    }
    getModel(name, params) {
        if (!params) {
            params = []
        }
        return this.factory.getModel(name, params)
    }

    getClass(name) {
        return this.factory.getClass(name)
    }
}

module.exports = App