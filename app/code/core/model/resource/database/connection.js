'use strict'



class Connection {
    /**
     * 
     * @param {*} adapter 
     */
    constructor(adapter) {
        if (adapter == null) {
            this.adapter =  Project.getModel('core/resource_database_adapter_mongo');
        }
    }

}

module.exports = Connection