'use strict'

const Abstract = Project.getClass('core/resource_database_adapter_abstract')

class Mongo extends Abstract {
    construct (host) {
        this.host = host
        this.client = require('mongodb').MongoClient
    }

    async connect() {

    }
}

module.exports = Mongo