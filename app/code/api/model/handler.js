'use strict'


class Handler {
    start() {
        try {
            let server = Project.getModel('web/server', 80)
            server.startServer()
        }
        catch(err) {
            Project.log(err)
        }
    }
}

module.exports = Handler