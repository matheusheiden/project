'use strict'

const DEFAULT_PORT = 80;

const http = require('http')

const net = require('net');

const url = require('url');

const DEFAULT_SCOPES = [
    'global',
    Project.scope
]

class Server {

    /**
     * 
     * @param {*} port 
     */
    constructor(port) {
        this.port = port ? port.pop() : DEFAULT_PORT //if port is not defined use default
        this._httpIntance = null
        

        this._router = Project.getModel('web/router')

        this.initiateRoutes()
    }


    startServer() {
        var self = this;
        this._httpIntance = http.createServer((req, res) => {
            self.handleRequest(req, res);
        });

        this._httpIntance.on('clientError', (err, socket) => {
            socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
        });

        this._httpIntance.listen(this.port);
    }

    handleRequest(req, res) {
        return this._router.route(req, res);
    }


    initiateRoutes () {
        var data = Project.getApp().module_data
        var self = this;
        let routes = []

        DEFAULT_SCOPES.forEach((scope) => {
            
            if (data.hasOwnProperty(scope) && data[scope].hasOwnProperty('web')) {
                routes.push(data[scope]['web'])
            }
        })

        routes.forEach(function(route) {
            self._router.addRoute(route)
        }) 
        
    }
}

module.exports = Server