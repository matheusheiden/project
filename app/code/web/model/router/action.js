'use strict'

class Action {

    constructor () {

    }

    set router(router) {
        this._router = router;
    }

    dispatchAction(action) {
        this.preDispatch();
        Project.log('Dispatching action: '+ action)

        this[action]()

        Project.log('Post Dispatch')

        this.postDispatch();
        

    }

    get request() {
        return this._router.request
    }

    get response() {
        return this._router.response
    }

    preDispatch()  {
        console.log(this.response)
    }

    postDispatch() {

    }

}

module.exports = Action