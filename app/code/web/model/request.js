'use strict'

class Request {
    
    constructor() {
        this.request = null
    }

    wrap(request) {
        this.request = request
        return this
    }

    getPath() {
        return this.request.url;
    }
}

module.exports = Request