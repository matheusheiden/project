'use strict'

class Response {
    constructor(){
        
    }
    wrap(response) {
        this.response = response
        return this
    }

    endStream(content) {
        if (!content)
            content = '' 

        this.response.end(content)
    }

    set status(def=200) {
        this.response.statusCode = def
        return this
    }
    
}

module.exports = Response