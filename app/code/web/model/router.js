'use strict'

class Router {
    constructor() {
        this._routes = []
    }

    addRoute(route) {
        this._routes.push(route)
        return this
    }

    route(req, res) {
        req = this._wrapRequest(req)
        res = this._wrapResponse(res)
        this._route(req, res).then(function(routingResult) {
            
            if (routingResult instanceof Error) {
                res.status = 404
                res.endStream('404')    
            }

            res.status = 200
            res.endStream()
        }).catch((reason) => {
            Project.log(reason)
            res.status = 500
            res.endStream('500');

        })
        
    }

    async _route(req, res) {
        this.request = req
        this.response = res
        let path = req.getPath()
        if (path.indexOf("?") !== -1) {
            path = path.substring(0, path.indexOf("?"))
        }
        path = path.substring(1)
        path = path+"/"
        console.log(path);
        let pathSplit = path.split("/")
        if (!pathSplit[1]) {
            pathSplit[1] = 'index'
        }
        var controller = this._loadController(pathSplit[0], pathSplit[1])

        if (!controller) {
            return new Error('Route not found')
        }

        if (!pathSplit[2]) {
            pathSplit[2] = 'index'
        }

        if (!typeof controller[pathSplit[2]+'Action'] == 'function') {
            return new Error('Route not found')
        }

        controller.router = this
        
        return controller.dispatchAction(pathSplit[2]+'Action')



    }

    _wrapRequest(requestData) {
        let request = Project.getModel('web/request')
        request.wrap(requestData)
        return request
    }

    _wrapResponse(response) {
        let res = Project.getModel('web/response')
        res.wrap(response)
        return res
    }

    _loadController(routeName, controllerName) {
        for (let key in this._routes) {
            if (this._routes[key].route.name == routeName) {
                try {
                    return Project.getApp().factory.getController(this._routes[key].route.moduleName+"/"+controllerName)
                }
                catch (err) {
                    Project.log(err)
                }
            }
        }
        return null
    }

}

module.exports = Router