'use strict'
const App = require.main.require('./app/code/core/model/app.js')

class Project {
    
    constructor (executionFlags)  {
        this.executionFlags = executionFlags
        this.app = new App();
        global.Project = this
    }

    run () {
        this.getApp().log("Starting application")
        this.app.start()
    }

    getApp() {
        return this.app
    }

    log(msg) {
        this.getApp().log(msg)
    }

    dispatchEvent (eventName, data) {
        this.getApp().dispatchEvent(eventName, data)
    }

    getModel(modelName, ...params) {
        try {
            return this.getApp().getModel(modelName, params)
        }
        catch (err) {
            console.log(err)
            //this.log(err.getMessage())
            return null
        }
    }

    getClass(modelName) {
        try {
            return this.getApp().getClass(modelName)
        }
        catch (err) {
            this.log(err.getMessage())
            return null
        }
    }
}

module.exports = Project